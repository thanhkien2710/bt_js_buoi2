//bai1
/* đầu vào: số tiền lương 1 ngày
    các bước sử lý:
    b1: tạo 1 biến chứa só tiền lương 1 ngày edge1
    b2:tạo 1 biến nhập ngày làm edge2
    b3: tạo 1 biến tính tiền lương result
    b4:gán giá trị cho edgj1,edgj2
    b5: tiền lương= lương một ngày *số ngày làm
    b6:in ra màn hình
    đầu ra:số tiền lương nhân viên
 */
function tinhTienLuong(){
    var ngayLamValue=document.getElementById("txt-ngay-lam").value*1;
    console.log("🚀 ~ file: index.js ~ line 3 ~ tinhTienLuong ~ ngayLamValue", ngayLamValue)
    var luongNgayValue=document.getElementById("txt-luong-ngay").value*1;
     console.log("🚀 ~ file: index.js ~ line 6 ~ tinhTienLuong ~ luongNgayValue", luongNgayValue)
    var tinhTienLuong=(ngayLamValue*luongNgayValue);
    console.log("🚀 ~ file: index.js ~ line 8 ~ tinhTienLuong ~ tinhTienLuong", tinhTienLuong)
    document.getElementById("result").innerHTML=` <h2 class="text-center">${tinhTienLuong}</h2>`;
}   
/*
bai2
đầu vào: 5 số thực
b1: tạo 5 biến chứa 5 số thực
b2:tạo biến tính giá trị trung bình
b3:gán giá trị cho 5 biến
b4:giá trị trung bình=(edge1+edge2+edge3+edge4+edge5)/5
b5:in ra màn hình
đầu ra: giá trị trung bình
 */
function tinhTrungBinh(){
    var ngayMotValue=document.getElementById("txt-ngay-mot").value*1;
    console.log("🚀 ~ file: index.js ~ line 14 ~ tinhTrungBinh ~ txt-ngay-mot", ngayMotValue)
    var ngayHaiValue=document.getElementById("txt-ngay-hai").value*1;
    console.log("🚀 ~ file: index.js ~ line 16 ~ tinhTrungBinh ~ ngayHaiValue", ngayHaiValue)
    var ngayBaValue=document.getElementById("txt-ngay-ba").value*1;
    console.log("🚀 ~ file: index.js ~ line 18 ~ tinhTrungBinh ~ ngayBaValue", ngayBaValue)
    var ngayBonValue=document.getElementById("txt-ngay-bon").value*1;
    console.log("🚀 ~ file: index.js ~ line 20 ~ tinhTrungBinh ~ ngayBonValue", ngayBonValue)
    var ngayNamValue=document.getElementById("txt-ngay-nam").value*1;
    console.log("🚀 ~ file: index.js ~ line 22 ~ tinhTrungBinh ~ ngayNamValue", ngayNamValue)
    var tinhTrungBinh=(ngayMotValue+ngayHaiValue+ngayBaValue+ngayBonValue+ngayNamValue)/5;
    console.log("🚀 ~ file: index.js ~ line 24 ~ tinhTrungBinh ~ tinhTrungBinh", tinhTrungBinh)
    document.getElementById("trungbinh").innerHTML=` <h2 class="text-center">${tinhTrungBinh}</h2>`;
}

/*
bai3
đầu vào: cho người dùng nhập số tiền usd
b1: tạo biến nhập số tiền
b2: tạo biến chứa giá trị tiền hiện nay
b3: gán giá trị cho 2 biến
b4:tính tiền= edge1*edge2
b5:in ra màn hình
 */
function doiTien(){
    var soTienValue=document.getElementById("txt-so-tien").value*1;
    console.log("🚀 ~ file: index.js ~ line 29 ~ doiTien ~ soTienValue", soTienValue)
    var doiTien=soTienValue*23500;
    console.log("🚀 ~ file: index.js ~ line 31 ~ doiTien ~ doiTien", doiTien)
    document.getElementById("doitien").innerHTML=`<h2 class="text-center">${doiTien}</h2>`;
}
/*
bai4
đầu vào: nhập chiều dài chiều rộng 
b1:tạo 2 biến chứa chiều dài edge1 và chiều rộng edge1
b2: tạo 2 biến tính chu vi edge4 và diện tích edge5
b3: gán giá trị cho chiều dài và rộng
b4: diện tích=dài*rộng,chu vi=(dài+rộng)*2
b5 in ra màn hình
 */
function tinhCVDT(){
    var chieuDaiValue=document.getElementById("txt-chieu-dai").value*1;
    console.log("🚀 ~ file: index.js ~ line 37 ~ tinhCVDT ~ chieuDaiValue", chieuDaiValue)
    var chieuRongValue=document.getElementById("txt-chieu-rong").value*1;
    console.log("🚀 ~ file: index.js ~ line 39 ~ tinhCVDT ~ chieuRongValue", chieuRongValue)
    var chuVi=(chieuDaiValue+chieuRongValue)*2;
    console.log("🚀 ~ file: index.js ~ line 41 ~ tinhCVDT ~ chuVi", chuVi)
    var dienTich=(chieuDaiValue*chieuRongValue);
    console.log("🚀 ~ file: index.js ~ line 43 ~ tinhCVDT ~ dienTich", dienTich)
    document.getElementById("chuvi").innerHTML=`<p>Chu vi là:</p>
                                               <h2 class=text-center>${chuVi}</h2> `;
    document.getElementById("dientich").innerHTML=`<p>Diện tích là:</p>
                                             <h2 class=text-center>${dienTich}</h2> `
}
/*
bai5
đầu vào: nhập vào 1 số có hai chữ số
b1:  tạo một biến số có hai chữ số n,biến hàng đơn vị và hàng chục
b2: gán giá trị cho n
b3:tách hàng đơn vị theo công thức: so%10
b4:tách hàng chục theo công thức:so/10
b5: in kết quả
đầu ra: kết quả sum của 2 chữ số đơn vị và hàng chục
 */
function nhapSo(){
    var nhapSoValue=document.getElementById("txt-so").value*1;
    console.log("🚀 ~ file: index.js ~ line 52 ~ nhapSo ~ nhapSoValue", nhapSoValue)
    var hangDonvi=nhapSoValue%10;
    console.log("🚀 ~ file: index.js ~ line 54 ~ nhapSo ~ hangDonvi", hangDonvi)
    var hangChuc=Math.floor(nhapSoValue/10)%10;
    console.log("🚀 ~ file: index.js ~ line 56 ~ nhapSo ~ hangChuc", hangChuc)
    var tong= hangChuc+hangDonvi;
    document.getElementById("tong").innerHTML=`<h2 class="text-center">${tong}</h2>`
}   